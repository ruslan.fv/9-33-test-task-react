import React from 'react';
import Header from './components/Header';
import Screen1 from "./components/Screen1";
import Screen2 from "./components/Screen2";
import Footer from "./components/Footer";

class App extends React.Component {
    constructor() {
        super();
        this.state = {first: true, rating: 0};
        this.changeState = this.changeState.bind(this);
    }

    changeState(first, rating) {
        this.setState({first: first, rating: rating}); // first property represents the current screen
    }

    render() {
        return (
            <div>
                <Header stars="3" reviews="159" appName="Some application name"/>
                <div className="content">
                    <div className="container text-center">
                        {this.state.first ? <Screen1 stars={this.state.rating} changeState={this.changeState}/> :
                            <Screen2 stars={this.state.rating} changeState={this.changeState}/>}
                    </div>
                </div>
                <Footer/>
            </div>
        );
    }
}

export default App;
