import React from 'react';

class Footer extends React.Component {
    render() {
        return (
            <div className="footer">
                <p className="copyright">(c) All rights reserved, 2018</p>
                <p className="links"><a href="#">Terms of use</a> - <a href="#">Privacy policy</a></p>
            </div>
        );
    }
}

export default Footer;