import React, {Component} from 'react';

function RatingStar(props) {
    return <span className={props.chosen ? "star chosen" : "star"}/>;
}

function RatingStarsBlock(props) {
    const stars = [];
    for (let i = 0; i < props.stars; i++) {
        stars.push(<RatingStar chosen={true} key={i}/>)
    }
    for (let i = props.stars; i < 5; i++) {
        stars.push(<RatingStar chosen={false} key={i}/>);
    }
    return (
        <div className="rating-container">
            <div className="rating-block">
                <p className="rating">{props.stars}.0</p>
                {stars}
            </div>
            <p className="reviews">{props.reviews} reviews</p>
        </div>);
}

class Header extends Component {
    render() {
        return (
            <div className="header">
                <div className="container">
                    <p className="title">{this.props.appName}</p>
                    <RatingStarsBlock stars={this.props.stars} reviews={this.props.reviews}/>
                </div>
            </div>
        );
    }
}

export default Header;
