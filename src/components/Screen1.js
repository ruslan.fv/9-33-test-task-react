import React from 'react';

let ratingText = 'How do you like the app?';
let finalRating = 0;


function RatingStar(props) {
    return <span onMouseMove={() => props.hover(props.id)} onClick={() => props.clickFunction(true, finalRating = props.id)}
                 className={props.rated ? "star rated" : "star"}
                 data-rating={props.id}/>;
}

function placeStars(active, onclick, onhover) {
    let stars = [];
    for (let i = 0; i < active; i++) {
        stars.push(<RatingStar hover={onhover} clickFunction={onclick} rated={true} id={i + 1} key={i}/>)
    }
    for (let i = active; i < 5; i++) {
        stars.push(<RatingStar hover={onhover} clickFunction={onclick} rated={false} id={i + 1} key={i}/>);
    }
    return stars;
}

class Screen1 extends React.Component {
    constructor(props) {
        super(props);
        this.state = {currentRating: this.props.stars};
        Screen1.hoverHandler = Screen1.hoverHandler.bind(this);
    }

    static hoverHandler(rating) {
        switch (rating) {
            case 1:
                ratingText = 'Very bad';
                break;
            case 2:
                ratingText = 'Bad';
                break;
            case 3:
                ratingText = 'Could be better';
                break;
            case 4:
                ratingText = 'Good';
                break;
            case 5:
                ratingText = 'Perfect!';
                break;
            default:
                ratingText = 'Choose the rating!';
                break;
        }
        this.props.changeState(true, rating);
    }


    render() {
        let stars = placeStars(finalRating, this.props.changeState, Screen1.hoverHandler);
        return (
            <div className="screen-1">
                <p className="title">Please, rate us!</p>
                <div className="rating-wrapper">
                    <div className="rating">
                        {stars}
                    </div>
                    <p className="value">{ratingText}</p>
                </div>
                <div className="button-wrapper">
                    <button onClick={() => this.props.changeState(false, finalRating)} // lifting the state up
                            className="button next-screen">Rate
                    </button>
                </div>
            </div>
        );
    }
}

export default Screen1;