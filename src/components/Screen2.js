import React from 'react';

class Screen2 extends React.Component {
    render() {
        let currentRating = this.props.stars;
        return (
            <div className="screen-2">
                <p className="title">Success!</p>

                <p className="description">
                    Thank you for your feedback! Tell us, why you gave us exactly {this.props.stars} stars
                    in any convenient way by posting a public review! Or you can <span className="reset"
                                                                                       onClick={() => this.props.changeState(true, currentRating)}>change</span> your
                    descicion.
                </p>

                <div className="button-wrapper">
                    <button className="button vk">VK Group</button>
                </div>
                <div className="button-wrapper">
                    <button className="button google-play">Google play</button>
                </div>
                <div className="button-wrapper">
                    <button className="button app-store">App store</button>
                </div>
                <a href="#" className="direct-reply">I don't have any of these but still want to write a review</a>
            </div>
        );
    }
}

export default Screen2;